from json import loads, dumps
from Chains import Blockchain

def WebsiteFromDict(jdict):
    location = jdict["location"]
    hashsum = jdict["hashsum"]
    return Website(location, hashsum)

def WebsiteFromString(jstring):
    return WebsiteFromDict(loads(jstring))

class Website:
    def __init__(self, name, location, hashsum):
        self.location = location
        self.hashsum = hashsum
        self.name = name

    def __dict__(self):
        return {"type": "web", "location": self.location, "hashsum": self.hashsum,
                "name": self.name}

    def __str__(self):
        return dumps(dict(self))

    def fromDict(jdict):
        return WebsiteFromDict(jstring)

    def fromString(jdata):
        return WebsiteFromString(jstring)

class WebsiteBlockchain(Blockchain):
    def __init__(self, *args):
        Blockchain.__init__(self, *args)

    def getWebsiteBlocks(self):
        return self.getBlocks().getType("web")

    def addWebsite(self, name, location, hashsum):
        page = Website(name, location, hashsum)
        self.appendObject("web", page.__dict__())

    def getWebsite(self, name):
        blocks = self.getWebsiteBlocks()
        for block in blocks:
            try:
                dat = block.body.value
                if dat['name'] == name:
                    return block
            except:
                continue
        return False
